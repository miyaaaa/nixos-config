{ config, pkgs, lib, ... }:

{
  boot = {
    #kernelParams = [ "quiet" ];
    #kernelPatches = [{}];
    #kernelPackages = "";
  };

  # TODO(miya): systemd-networkd, systemd-resolvd, dhcpcd?
  networking = {
    dhcpcd.enable = false;
    useDHCP = false;
    useNetworkd = true;
    firewall.enable = true;
    enableIPv6 = true;
    #wireless.enable = true;
  };

  systemd.network.enable = true;

  systemd.network.networks = let
    networkConfig = {
      DHCP = "yes";
      DNSSEC = "yes";
      DNSOverTLS = "yes";
      DNS = [ "9.9.9.9" "149.112.112.112" ];
    };
  in {
    # Config for all useful interfaces
    "40-wired" = {
      enable = true;
      name = "en*";
      inherit networkConfig;
      dhcpV4Config.RouteMetric = 1024;
    };
    "40-wireless" = {
      enable = true;
      name = "wl*";
      inherit networkConfig;
      dhcpV4Config.RouteMetric = 2048; # Prefer wired.
    };
  };

  hardware = {
    opengl.driSupport32Bit = true;
  };

  # Enable Bluetooth support.
  #hardware.bluetooth = {
  #  enable = true;
  #  package = pkgs.pkgsUnstable.bluez;
  #};

  # Use Pipewire.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    media-session.enable = true;
  };

  virtualisation.docker = {
    enable = true;
    enableOnBoot = false;
  };
  virtualisation.libvirtd = {
    enable = true;
  };
}
