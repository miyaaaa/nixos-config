{ config, lib, pkgs, ... }:

{
  # Allow only these unfree packages.
  nixpkgs.config.allowUnfreePredicate = pkg:
    pkgs.lib.elem (if (builtins.hasAttr "name" pkg) then (builtins.parseDrvName pkg.name).name else pkg.pname)
    [ "steam" "steam-original" "steam-runtime" "spotify" "nvidia-x11" "nvidia-settings" ];

  environment.systemPackages = with pkgs; [
    # Basic tools
    pciutils
    wget curl file ripgrep fd

    # Version control
    git gh

    # Utilities
    qemu pandoc clang-tools rustfmt

    # X utilities
    xclip xsel flameshot gromit-mpx

    # Build system
    cmake

    # Libraries

    # Programming Languages
    cargo go

    # Terminal and editor
    kitty zsh tmux neovim

    # Browser
    firefox

    # Cloud and sync
    #syncthing # TODO(miya): Install package on a per-user basis?

    # GTK+ and icon theme

    # Office suite

    # Visual editors
    gimp mypaint

    # CLI A/V/I editors
    exiftool

    # Multimedia

    # Networking
    openssh openvpn

    # WM utilities

    # System utilities
    lsd pavucontrol htop docker docker-compose
  ];
}
