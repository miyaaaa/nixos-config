{ config, pkgs, ... }:

{
  services.xserver = {
    # Enable the X11 windowing system.
    enable = true;

    # Disable auto start at boot time.
    # Start manually: `systemctl start display-manager.service`.
    #autorun = false;

    # Keyboard options.
    layout = "us";
    xkbVariant = "dvorak,dvp";
    xkbOptions = "caps:escape,compose:ralt";

    # Enable the lightdm login manager
    displayManager = {
      lightdm = {
        enable = true;
      };
      defaultSession = "none+dwm"; # none+bspwm
    };

    # bspwm window manager.
    windowManager = {
      # default = "dwm";
      bspwm = {
        enable = true;
        configFile = ../dotfiles/bin/bspwmrc;
      };
      dwm = {
        enable = true;
      };
    };
  };

  environment.variables = {
    #GTK_THEME= "Nordic";
    #GTK_ICON_THEME = "Paper";
  };

  environment.extraInit = ''
    #export XDG_CONFIG_DIRS="/etc/xdg:$XDC_CONFIG_DIRS"
    #export RUST_BACKTRACE=1
  '';

  fonts = {
    fonts = with pkgs; [
      dejavu_fonts
      ubuntu_font_family
      jetbrains-mono
    ];
    fontconfig = {
        #hinting.enable = false;
        #subpixel.rgba = "none";
        #subpixel.lcdfilter = "none";
    };
  };
}
