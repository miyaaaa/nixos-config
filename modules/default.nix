{ config, pkgs, ... }:

{
  imports = [
    ./hardware.nix
    ./packages.nix
    ./shell.nix
    ./users.nix
    ./xserver.nix
    #./wayland.nix
  ];

  nixpkgs.overlays = import ../packages/overlays;

  #nix = {
  #    useSandbox = true;
  #    extraOptions = ''
  #        fallback = true
  #    '';
  #};

  # Internationalisation properties.
  # TODO(miya): programmer-dvorak (dvp) does not seem to exist. Do I need to
  # make my own keymap?
  console.keyMap = "dvorak";
  i18n.defaultLocale = "en_US.UTF-8";

  # Time zone.
  time.timeZone = "Europe/Zurich";

  # NixOS release version.
  system.stateVersion = "21.05";
}
