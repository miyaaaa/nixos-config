{ config, pkgs, ... }:

{
  # Disable use of `useradd`, `userdel`, etc...
  #users.mutableUsers = false;

  # Allow passwordless sudo for wheel users.
  #security.sudo.wheelNeedsPassword = false;

  # User accounts.
  users = {
    extraUsers = {
      miya = {
        isNormalUser = true;
        uid = 1000;
        extraGroups = [
          "wheel" "docker" "kvm"
        ];
        #hashedPassword = "$6$5F3RLa4dG$690cWpXpuZVnv8WmHVwkQCFmlEBcEg3W8CTaxrp8uwYd9vpvETrPfri7tJpcXiEFdJFybBwq63ooWzaP3kUZ/0";
        home = "/home/miya";
        shell = pkgs.zsh;
      };

      # Disable login as root.
      #root = {
      #  hashedPassword = "*";
      #};
    };
  };

  # SSH config.
  programs.ssh.extraConfig = builtins.readFile ../dotfiles/ssh-config;
}
