{ config, pkgs, lib, ... }:

{
  environment.variables = {
    EDITOR = lib.mkOverride 900 "nvim";
    TERMINAL = "kitty";
    # TODO(miya): zsh env vars?
  };

  programs.zsh = {
    enable = true;
    shellAliases = {
      vim = "nvim";
      ls = "lsd";
      lt = "ls --tree";
      l = "ls -lahF";
      yo = "yt-dlp";
      scramble = "exiftool -all=";
      qemu = "qemu-system-x86_64 -bios /usr/share/edk2-ovmf/OVMF_CODE.fd -m 16G -boot order=dc -enable-kvm -smp (nproc --all)";
    };

    ohMyZsh = {
      enable = true;
      plugins = [ "git" "zsh-syntax-highlighting" "zsh-autosuggestions" "autojump" "thefuck" ];
      theme = "clean"; # awesomepanda
    };
  };
}
