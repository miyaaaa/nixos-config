{ config, pkgs, ... }:

{
  imports = [
    # import other config files/directories
  ];

  boot = {
    loader = {
      # Use the systemd-boot EFI boot loader.
      systemd-boot = {
        enable = true;
        consoleMode = "max";
        editor = false; # Disable editing the kernel command-line.
      };
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot";
      timeout = 10;
    };

    initrd.availableKernelModules = [ "sd_mod" "sr_mod" ]; # TODO: Other modules?
    kernelModules = [ ];
    kernelParams = [ ];
    extraModulePackages = [ ];
    #kernelPatches = [ { } ];
  };

  networking.hostName = "catboy";

  services.xserver = {
    #videoDrivers = [ "modesetting" /*"nvidia"*/ ];

    # Set autologin
    #displayManager.autoLogin = {
    #  enable = true;
    #  user = "miya";
    #};
  };
}
