# Replace with auto generated `nixos-generate-config`.
boot.initrd.availableKernelModules = [ "sd_mod" "sr_mod" ];
boot.initrd.kernelModules = [ ];
boot.kernelModules = [ ];
boot.extraModulePackages = [ ];

fileSystems."/" =
  { device = "/dev/disk/by-label/nixos_root";
    fsType = "ext4";
  };

fileSystems."/boot" =
  { device = "/dev/disk/by-label/nixos_boot";
    fsType = "vfat";
  };

fileSystems."/home" =
  { device = "/dev/disk/by-label/nixos_home";
    fsType = "btrfs";
  };

#swapDevices = 
#  [ { device = "/dev/disk/by-label/nixos_swap"; }
#  ];
swapDevices = [ ];
