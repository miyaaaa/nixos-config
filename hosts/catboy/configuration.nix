{ config, pkgs, ... }:

{
  imports = [
    # import other config files/directories
  ];

  boot = {
    loader = {
      # Use the systemd-boot EFI boot loader.
      systemd-boot = {
        enable = true;
        consoleMode = "max";
        editor = false; # Disable editing the kernel command-line.
      };
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot";
      timeout = 10;
    };

    kernelParams = [ ];
    #kernelPatches = [ { } ];
  };

  networking.hostName = "catboy";

  environment.systemPackages = with pkgs; [
    syncthing
  ];

  services.xserver = {
    videoDrivers = [ /*"nouveau"*/ "nvidia" ];

    # Set autologin
    #displayManager.autoLogin = {
    #  enable = true;
    #  user = "miya";
    #};
  };
}
