# NixOS Configuration

My NixOS configuration.

## Setup

```shell
# Generate default config.
nixos-generate-config --root /mnt

# Install config.
nix-env -iA nixos.git
cd /mnt/etc/nixos/
git clone https://gitlab.com/miyaaaa/nixos-config.git
mv nixos-config/* nixos-config/.* .
rmdir nixos-config
ln -s hosts/{HOSTNAME} host

# Replace hardware config.
mv hardware-configuration.nix host/

# Install nixos.
nixos-install
```

## Directory structure

```
  .
├──   dotfiles
│  └──   bin
├──   modules
├──   hosts
└──   packages
   ├──   custom
   ├──   overlays
   └──   overrides
```

Directory          | Description
------------------ | -----------
dotfiles           | Contains all dotfiles.
dotfiles/bin       | Executable dotfiles `#!/bin/sh`.
modules            | Nix system configuration.
hosts              | Host configuration.
packages           | Package configuration.
packages/custom    | Custom packages.
packages/overrides | Overridden/modified packages.
packages/overlays  | Package overlays to include multiple packages at once. (e.g. include all overridden packages.)
