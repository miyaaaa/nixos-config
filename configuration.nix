{
  imports = [
    ./host/configuration.nix
    ./host/hardware-configuration.nix
    ./modules
  ];
}
